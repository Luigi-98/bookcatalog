LibCatalog
==========

## N.B.
The code is working, but also very old, it's mostly an early youth experiment, so all the credits to the 10-years-ago Luigi Privitera.

## How to set a language
Before building go to ./src/lang.h and set a number corresponding to the language you prefer:

1. English
2. Italian

You will find the line

`#define _LANGUAGE 1`

That's the number to change.

## How to build
Run the following

`$ make`

Now ./libcatalog is your executable and will use library.inf as database.