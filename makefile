CC=gcc
OUT=./libcatalog
CFLAGS=-O3
SRC=./src

$(OUT): $(SRC)/main.c $(SRC)/utils.h $(SRC)/file.h $(SRC)/books.h $(SRC)/lang.h
	$(CC) $(SRC)/main.c -o $(OUT) $(CFLAGS)

clear:
	rm $(OUT)

all: clear $(OUT)