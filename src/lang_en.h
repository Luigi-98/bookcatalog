#define INTERFACE_QUESTION "What do you want to do? (h for help) "
#define INTERFACE_WANNACREATE "Do you want to create a new database?"
#define INTERFACE_WANNADELETE "Are you sure you want to delete this database?"

#define INTERFACE_HELP "\
Help:\n\
l\tList books in database\n\
d\tDelete actual database\n\
a\tAdd book to database\n\
s\tSave dataset\n\
h\tShow this help\n\
q\tQuit\n"

#define INTERFACE_FILENAME "Filename"

#define INTERFACE_AUTHOR "Author"
#define INTERFACE_TITLE "Title"
#define INTERFACE_PRINTINGYEAR "PrintingYear"
#define INTERFACE_EDITOR "Editor"
#define INTERFACE_PLACE "Place"
#define INTERFACE_REVISION "Rev"
#define INTERFACE_FUND "Fund"
#define INTERFACE_BOOKID "Book id"
#define INTERFACE_SURETOADD "Sure to add this?"

#define INTERFACE_SAVED "Database saved!"
#define INTERFACE_NOTSAVED "Could not save database!"

#define INTERFACE_SURETOEXITWITHOUTSAVING "Sure to quit without saving?"