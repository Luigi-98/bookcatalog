#include "books.h"
#include "lang.h"
#include <stdlib.h>

#define FILENAME "library.inf"

int main()
{
	char c;
	if (load(FILENAME)==ERROR)
	{
		printf("%s ([Y]/n) ",INTERFACE_WANNACREATE);
		c=cleaned_getchar();
		if (c=='n')
			return EXIT_FAILURE;
		new_database();
	}
	c='\0';
	while (c!='q')
	{
		printf(INTERFACE_QUESTION);
		c=cleaned_getchar();
		switch (c)
		{
			case 'd':
				printf("%s (Y/[n]) ",INTERFACE_WANNADELETE);
				c=cleaned_getchar();
				if (c=='Y') delete_database(FILENAME);
				return main();
				break;
			case 'l':
				listBooks();
				break;
			case 'a':
				addBook();
				break;
			case 's':
				if (save(FILENAME)==OK)
					printf("%s\n",INTERFACE_SAVED);
				else
					printf("%s\n",INTERFACE_NOTSAVED);
				break;
			case 'h':
			case '\n':
				printf(INTERFACE_HELP);
				printf("\n%s: %s\n", INTERFACE_FILENAME, FILENAME);
				break;
			case 'q':
				if (modified)
				{
					printf("%s (Y/[n]) ",INTERFACE_SURETOEXITWITHOUTSAVING);
					c=cleaned_getchar();
					if (c=='Y') c='q';
				}
				break;
		}
	}

	return EXIT_SUCCESS;
}
