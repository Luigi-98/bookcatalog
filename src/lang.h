#ifndef _LANG
#define _LANG

#define _LANGUAGE 1

#if _LANGUAGE==2
#include "lang_it.h"
#else
#include "lang_en.h"
#endif

#endif