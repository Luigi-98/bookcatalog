#include "lang.h"
#include "file.h"
#include "utils.h"
#define MAXBOOKS 1000
#define VERYLONG 1000
#define LONG 150
#define SHORT 50
#define VERYSHORT 20

typedef struct {
	char 	title[LONG],//Title:
			author[SHORT],//Author:
			editor[SHORT],//Editor:
			place[VERYSHORT], //Place:
			rev[SHORT], //Rev:
			fund[VERYSHORT]; //Fund:
	uint printYear;
	uint id;
} Book;

Book books[MAXBOOKS];
int nBooks=0;
uint maxid=0;

uint loaded=FALSE;
uint modified=FALSE;

int loader(char* filename)
{
	char* raw=fileRead(filename);
	if (raw==NULL) return ERROR;
	
	char buff[MAXLEN], buff2[VERYSHORT];
	char* buffPt=buff;
	
	for (char* c=raw; *c!='\0'; c++)
	{
		for (;*c!='#';c++) if (*c=='\0') return OK;
		
		//getid
		for (buffPt=buff,c++; *c!=':'; buffPt++,c++)
		{
			if (*c=='\0'||*c=='\n') return ERROR+2;
			*buffPt=*c;
		}
		*buffPt='\0';
		maxid=MAX(maxid,books[nBooks].id=stringInt(buff));
		
		c++; //skip ':'
		
		//getfeatures
		for (;*c!='.'&&*c!='\0';)
		{
			c++; //skip '\n'
			
			//get tag name
			for (*buff2='\0',buffPt=buff2; *c!=':'; *(buffPt++)=*(c++))	if (*c=='\0'||*c=='\n'||(buffPt-buff2)>=14) return ERROR+3;
			*buffPt='\0';
			
			c++; //skip ':'
			
			//get tag content
			for (*buff='\0',buffPt=buff; *c!=';'; *(buffPt++)=*(c++)) if (*c=='\0'||*c=='\n') return ERROR+4;
			*buffPt='\0';
			if (stringEq(buff2,"Author")) stringCopy(buff,books[nBooks].author);
				else if (stringEq(buff2,"Title")) stringCopy(buff,books[nBooks].title);
				else if (stringEq(buff2,"PrintingYear")) books[nBooks].printYear=stringInt(buff);
				else if (stringEq(buff2,"Editor")) stringCopy(buff,books[nBooks].editor);
				else if (stringEq(buff2,"Place")) stringCopy(buff,books[nBooks].place);
				else if (stringEq(buff2,"Rev")) stringCopy(buff,books[nBooks].rev);
				else if (stringEq(buff2,"Fund")) stringCopy(buff,books[nBooks].fund);
			c++; //skip ';'
		}
		nBooks++;
	}
	return OK;
}

int new_database()
{
	loaded=TRUE;
	modified=TRUE;
	return OK;
}

void (*delete_database)(char*) = fileDelete;

int load(char* filename)
{
	if (loader(filename)==OK)
	{
		loaded=TRUE;
		modified=FALSE;
		return OK;
	}
	loaded=FALSE;
	modified=FALSE;
	return ERROR;
}

int listBooks()
{
	if (!loaded) return ERROR;
	for (int i=0; i<nBooks; i++)
	{
		printf("%s: %d\n\
			%s: %s\n\
			%s: %s\n\
			%s: %d\n\
			%s: %s\n\
			%s: %s\n\
			%s: %s\n\
			%s: %s\n",
			INTERFACE_BOOKID,books[i].id,
			INTERFACE_TITLE,books[i].title,
			INTERFACE_AUTHOR,books[i].author,
			INTERFACE_PRINTINGYEAR,books[i].printYear,
			INTERFACE_PLACE,books[i].place,
			INTERFACE_EDITOR,books[i].editor,
			INTERFACE_REVISION,books[i].rev,
			INTERFACE_FUND,books[i].fund);
	}
	return OK;
}

int addBook()
{
	char 	title[LONG],
			author[SHORT],
			editor[SHORT],
			place[VERYSHORT],
			rev[SHORT],
			fund[VERYSHORT],
			printYear[VERYSHORT];
	char c;
	
	printf("%s: ", INTERFACE_TITLE);
	stringCopy(input(),title);
	printf("%s: ", INTERFACE_AUTHOR);
	stringCopy(input(),author);
	printf("%s: ", INTERFACE_PRINTINGYEAR);
	stringCopy(input(),printYear);
	printf("%s: ", INTERFACE_PLACE);
	stringCopy(input(),place);
	printf("%s: ", INTERFACE_EDITOR);
	stringCopy(input(),editor);
	printf("%s: ", INTERFACE_REVISION);
	stringCopy(input(),rev);
	printf("%s: ", INTERFACE_FUND);
	stringCopy(input(),fund);
	
	printf("%s ([Y]/n) ",INTERFACE_SURETOADD);
	c=cleaned_getchar();
	if (c=='n') return ERROR;

	trim(title);
	trim(author);
	trim(printYear);
	trim(place);
	trim(editor);
	trim(rev);
	trim(fund);
	
	stringCopy(title,books[nBooks].title);
	stringCopy(author,books[nBooks].author);
	stringCopy(place,books[nBooks].place);
	stringCopy(editor,books[nBooks].editor);
	stringCopy(rev,books[nBooks].rev);
	stringCopy(fund,books[nBooks].fund);
	books[nBooks].printYear=stringInt(printYear);
	books[nBooks].id=(++maxid);
	
	nBooks++;
	
	loaded=TRUE;
	modified=TRUE;
	
	return OK;
}

int save(char* filename)
{
	if (!loaded) return ERROR;
	if (!modified) return OK;
	char buff[MAXLEN];
	char* buffPt=buff;
	for (int i=0; i<nBooks; i++)
	{
		sprintf(buffPt,"#%d:\nTitle:%s;\nAuthor:%s;\nPrintingYear:%d;\nEditor:%s;\nPlace:%s;\nRev:%s;\nFund:%s;.\n",books[i].id,books[i].title,books[i].author,books[i].printYear,books[i].editor,books[i].place,books[i].rev,books[i].fund);
		for (;*buffPt!='\0';buffPt++);
	}
	fileWrite(filename,buff);
	modified=FALSE;
	return OK;
}
