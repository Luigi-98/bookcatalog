#define INTERFACE_QUESTION "Cosa vuoi fare? (h per aiuto) "
#define INTERFACE_WANNACREATE "Vuoi creare un nuovo database?"
#define INTERFACE_WANNADELETE "Sicuro di voler cancellare questo database?"

#define INTERFACE_HELP "\
Aiuto:\n\
l\tMostra i libri nel database\n\
d\tCancella il database attuale\n\
a\tAggiungi un libro al database\n\
s\tSalva il database\n\
h\tMostra questo aiuto\n\
q\tChiudi\n"

#define INTERFACE_FILENAME "Nome file"

#define INTERFACE_AUTHOR "Autore"
#define INTERFACE_TITLE "Titolo"
#define INTERFACE_PRINTINGYEAR "Anno di stampa"
#define INTERFACE_EDITOR "Editore"
#define INTERFACE_PLACE "Luogo"
#define INTERFACE_REVISION "Rev"
#define INTERFACE_FUND "Fondo"
#define INTERFACE_BOOKID "Id libro"
#define INTERFACE_SURETOADD "Confermi di aggiungere?"

#define INTERFACE_SAVED "Database salvato!"
#define INTERFACE_NOTSAVED "Non sono riuscito a salvare il database!"

#define INTERFACE_SURETOEXITWITHOUTSAVING "Sicuro di voler uscire senza salvare?"