#define ASCII0 48
#define TRUE 1
#define FALSE 0
#define MAX(a,b) (((a)>(b))?(a):(b))

int stringInt(char* str)
{
	int res=0;
	for (; *str!='\0'; res=res*10+(*str-ASCII0),str++);
	return res;
}

int stringEq(char* a, char* b)
{
	while (1)
	{
		if (*(a++)==*(b++)) {
			if (*a=='\0') return TRUE;
		} else return FALSE;
	}
}

void memoryCopy(char* inp, char* out, int len)
{
	for (; len--; *(out++)=*(inp++));
}

void stringCopy(char* inp, char* out)
{
	for (; *inp!='\0'; *(out++)=*(inp++));
	*out='\0';
}

int stringLen(char* str)
{
	char* str_cur=str-1;
	while (*(++str_cur)!='\0');
	return str_cur-str;
}

char* input()
{
	char buff[MAXLEN];
	char* pt=buff-1;
	do {
		*(++pt)=getchar();
	} while (*pt!='\0'&&*pt!='\n');
	*(pt)='\0';
	char* res=(char*) malloc(stringLen(buff)+1);
	stringCopy(buff,res);
	return res;
}

char cleaned_getchar()
{
	char c,q;
	c=getchar();
	q=c;
	while (q!=EOF&&q!='\0'&&q!='\n')
		q=getchar();
	return c;
}

void trim(char* inp)
{
	char* inpBak=inp;
	char* res=(char*) malloc(MAXLEN*sizeof(char));
	char* resBak=res;
	for (; *inp==' '||*inp=='\n'; inp++);
	for (; *inp!='\0'; *(res++)=*(inp++));
	for (; *res==' '||*res=='\n'; res--);
	*res='\0';
	stringCopy(resBak,inpBak);
	return;
}