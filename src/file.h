#include<stdio.h>
#include<stdlib.h>
#define MAXLEN 100000
#define ERROR 255
#define OK 0

char* fileRead(char* filename)
{
	char* res=(char*)malloc(sizeof(char)*MAXLEN);
	
	FILE* f = fopen(filename, "r");
	if (f==NULL)
	{
		free(res);
		return NULL;
	}
	
	int i;
	for (i=0; (res[i]=fgetc(f))!=EOF; i++)
	{
		if (i>=MAXLEN-1)
		{
			printf("Too long a file!\n");
			fclose(f);
			return 0;
		}
	}
	res[i]='\0';
	return (fclose(f)==0)?res:"";
}

int fileWrite(char* filename, char* newContent)
{
	FILE* f = fopen(filename, "w");
	if (f==NULL) return ERROR;
	return (fputs(newContent,f)>=0)?fclose(f):ERROR;
}

void fileDelete(char* filename)
{
	char cmd[MAXLEN];
	sprintf(cmd, "rm \"%s\"", filename);
	system(cmd);
}